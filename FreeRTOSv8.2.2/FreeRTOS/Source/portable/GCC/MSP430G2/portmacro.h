/*
    FreeRTOS V8.2.2 - Copyright (C) 2015 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation >>!AND MODIFIED BY!<< the FreeRTOS exception.

    ***************************************************************************
    >>!   NOTE: The modification to the GPL is included to allow you to     !<<
    >>!   distribute a combined work that includes FreeRTOS without being   !<<
    >>!   obliged to provide the source code for proprietary components     !<<
    >>!   outside of the FreeRTOS kernel.                                   !<<
    ***************************************************************************

    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  Full license text is available on the following
    link: http://www.freertos.org/a00114.html

    ***************************************************************************
     *                                                                       *
     *    FreeRTOS provides completely free yet professionally developed,    *
     *    robust, strictly quality controlled, supported, and cross          *
     *    platform software that is more than just the market leader, it     *
     *    is the industry's de facto standard.                               *
     *                                                                       *
     *    Help yourself get started quickly while simultaneously helping     *
     *    to support the FreeRTOS project by purchasing a FreeRTOS           *
     *    tutorial book, reference manual, or both:                          *
     *    http://www.FreeRTOS.org/Documentation                              *
     *                                                                       *
    ***************************************************************************

    http://www.FreeRTOS.org/FAQHelp.html - Having a problem?  Start by reading
    the FAQ page "My application does not run, what could be wrong?".  Have you
    defined configASSERT()?

    http://www.FreeRTOS.org/support - In return for receiving this top quality
    embedded software for free we request you assist our global community by
    participating in the support forum.

    http://www.FreeRTOS.org/training - Investing in training allows your team to
    be as productive as possible as early as possible.  Now you can receive
    FreeRTOS training directly from Richard Barry, CEO of Real Time Engineers
    Ltd, and the world's leading authority on the world's leading RTOS.

    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool, a DOS
    compatible FAT file system, and our tiny thread aware UDP/IP stack.

    http://www.FreeRTOS.org/labs - Where new FreeRTOS products go to incubate.
    Come and try FreeRTOS+TCP, our new open source TCP/IP stack for FreeRTOS.

    http://www.OpenRTOS.com - Real Time Engineers ltd. license FreeRTOS to High
    Integrity Systems ltd. to sell under the OpenRTOS brand.  Low cost OpenRTOS
    licenses offer ticketed support, indemnification and commercial middleware.

    http://www.SafeRTOS.com - High Integrity Systems also provide a safety
    engineered and independently SIL3 certified version for use in safety and
    mission critical applications that require provable dependability.

    1 tab == 4 spaces!
*/

#ifndef PORTMACRO_H
#define PORTMACRO_H

#include <msp430.h>

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------
 * Port specific definitions.
 *
 * The settings in this file configure FreeRTOS correctly for the
 * given hardware and compiler.
 *
 * These settings should not be altered.
 *-----------------------------------------------------------
 */

/* Type definitions. */
#define portCHAR	char
#define portFLOAT	float
#define portDOUBLE	double
#define portLONG	long
#define portSHORT	int
#define portSTACK_TYPE	uint16_t
#define portBASE_TYPE	short

typedef portSTACK_TYPE StackType_t;
typedef short BaseType_t;
typedef unsigned short UBaseType_t;

/* Beware: this is only valid for data pointers, not function pointers */
#define portPOINTER_SIZE_TYPE uintptr_t

#if __MSP430X__ & ( __MSP430_CPUX_TARGET_SR20__ | __MSP430_CPUX_TARGET_ISR20__ )
typedef uint20_t SavedRegister_t;
#else /* __MSP430X__ */
typedef BaseType_t  SavedRegister_t;
#endif /* __MSP430X__ */

#if( configUSE_16_BIT_TICKS == 1 )
    typedef uint16_t TickType_t;
    #define portMAX_DELAY ( TickType_t ) 0xffff
#else
    typedef uint32_t TickType_t;
    #define portMAX_DELAY ( TickType_t ) 0xffffffffUL
#endif

/* Stack sizes are in words, but a major component can be the TCB.
 * Since the size of the TCB depends on the memory model, abstract it.
 * Keep in mind that the memory model also affects the size of return
 * addresses.  Also consider adding space for two TCBs if the task has
 * interrupts enabled, since an interrupt handler will need space for
 * essentially the same information as a TCB, plus whatever its local
 * stack requirements may be. */
#define portTCB_SIZE_WORDS (3+12*sizeof(SavedRegister_t)/sizeof(portBASE_TYPE))

/* %u will not print a long int */
#define portLU_PRINTF_SPECIFIER_REQUIRED 1
    
/*-----------------------------------------------------------*/

/* Interrupt control macros. */
#define portDISABLE_INTERRUPTS()    asm volatile ( "DINT" ); asm volatile ( "NOP" )
#define portENABLE_INTERRUPTS()     asm volatile ( "EINT" ); asm volatile ( "NOP" )
    
/*-----------------------------------------------------------*/

/* Critical section control macros. */
#define portNO_CRITICAL_SECTION_NESTING         ( ( uint16_t ) 0 )

#define portENTER_CRITICAL()                                                \
{                                                                           \
    extern volatile uint16_t usCriticalNesting;                             \
                                                                            \
    portDISABLE_INTERRUPTS();                                               \
                                                                            \
    /* Now interrupts are disabled ulCriticalNesting can be accessed */     \
    /* directly.  Increment ulCriticalNesting to keep a count of how many */\
    /* times portENTER_CRITICAL() has been called. */                       \
    usCriticalNesting++;                                                    \
}

#define portEXIT_CRITICAL()                                                     \
{                                                                               \
extern volatile uint16_t usCriticalNesting;                                     \
                                                                                \
    if( usCriticalNesting > portNO_CRITICAL_SECTION_NESTING )                   \
    {                                                                           \
        /* Decrement the nesting count as we are leaving a critical section. */	\
        usCriticalNesting--;                                                    \
                                                                                \
        /* If the nesting level has reached zero then interrupts should be */   \
        /* re-enabled. */                                                       \
        if( usCriticalNesting == portNO_CRITICAL_SECTION_NESTING )              \
        {                                                                       \
                portENABLE_INTERRUPTS();                                        \
        }                                                                       \
    }                                                                           \
}
    
/*-----------------------------------------------------------*/

/* Task utilities. */
extern void vPortYield( void ) __attribute__ ( ( naked ) );
#define portYIELD()			vPortYield()
#define portNOP()			asm volatile ( "NOP" )
/*-----------------------------------------------------------*/

/* Hardwware specifics. */
#define portBYTE_ALIGNMENT  2
#define portSTACK_GROWTH    ( -1 )
#define portTICK_PERIOD_MS  ( ( TickType_t ) 1000 / configTICK_RATE_HZ )
/*-----------------------------------------------------------*/

/* Task function macros as described on the FreeRTOS.org WEB site. */
#define portTASK_FUNCTION_PROTO( vFunction, pvParameters ) void vFunction( void *pvParameters )
#define portTASK_FUNCTION( vFunction, pvParameters ) void vFunction( void *pvParameters )

#if ! defined( portDISABLE_FLL )
#if defined(__MSP430_HAS_UCS__) || defined(__MSP430_HAS_UCS_RF__)
#define portDISABLE_FLL 1
#endif
#endif

#if ! defined( portLPM_bits )
#if portDISABLE_FLL
/* Exclude SCG0 from bits cleared on return, so FLL remains
 * disabled. */
#define portLPM_bits ( LPM4_bits & ~SCG0 )
#else /* portDISABLE_FLL */
#define portLPM_bits LPM4_bits
#endif /* portDISABLE_FLL */
#endif /* portLPM_bits */

#ifdef __cplusplus
}
#endif

#endif /* PORTMACRO_H */


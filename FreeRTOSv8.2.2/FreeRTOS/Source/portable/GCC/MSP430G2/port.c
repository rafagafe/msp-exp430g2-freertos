/*
    FreeRTOS V8.2.2 - Copyright (C) 2015 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation >>!AND MODIFIED BY!<< the FreeRTOS exception.

    ***************************************************************************
    >>!   NOTE: The modification to the GPL is included to allow you to     !<<
    >>!   distribute a combined work that includes FreeRTOS without being   !<<
    >>!   obliged to provide the source code for proprietary components     !<<
    >>!   outside of the FreeRTOS kernel.                                   !<<
    ***************************************************************************

    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  Full license text is available on the following
    link: http://www.freertos.org/a00114.html

    ***************************************************************************
     *                                                                       *
     *    FreeRTOS provides completely free yet professionally developed,    *
     *    robust, strictly quality controlled, supported, and cross          *
     *    platform software that is more than just the market leader, it     *
     *    is the industry's de facto standard.                               *
     *                                                                       *
     *    Help yourself get started quickly while simultaneously helping     *
     *    to support the FreeRTOS project by purchasing a FreeRTOS           *
     *    tutorial book, reference manual, or both:                          *
     *    http://www.FreeRTOS.org/Documentation                              *
     *                                                                       *
    ***************************************************************************

    http://www.FreeRTOS.org/FAQHelp.html - Having a problem?  Start by reading
    the FAQ page "My application does not run, what could be wrong?".  Have you
    defined configASSERT()?

    http://www.FreeRTOS.org/support - In return for receiving this top quality
    embedded software for free we request you assist our global community by
    participating in the support forum.

    http://www.FreeRTOS.org/training - Investing in training allows your team to
    be as productive as possible as early as possible.  Now you can receive
    FreeRTOS training directly from Richard Barry, CEO of Real Time Engineers
    Ltd, and the world's leading authority on the world's leading RTOS.

    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool, a DOS
    compatible FAT file system, and our tiny thread aware UDP/IP stack.

    http://www.FreeRTOS.org/labs - Where new FreeRTOS products go to incubate.
    Come and try FreeRTOS+TCP, our new open source TCP/IP stack for FreeRTOS.

    http://www.OpenRTOS.com - Real Time Engineers ltd. license FreeRTOS to High
    Integrity Systems ltd. to sell under the OpenRTOS brand.  Low cost OpenRTOS
    licenses offer ticketed support, indemnification and commercial middleware.

    http://www.SafeRTOS.com - High Integrity Systems also provide a safety
    engineered and independently SIL3 certified version for use in safety and
    mission critical applications that require provable dependability.

    1 tab == 4 spaces!
*/

/*
	Changes from V2.5.2
		
	+ usCriticalNesting now has a volatile qualifier.
*/

/* Standard includes. */
#include <stdlib.h>
#include <legacymsp430.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"


/*-----------------------------------------------------------
 * Implementation of functions defined in portable.h for the MSP430 port.
 *----------------------------------------------------------*/

/* Constants required for hardware setup.  */
#if portDISABLE_FLL
#define portTASK_INITIAL_R2	( ( portBASE_TYPE ) ( GIE | SCG0 ) )
#else /* portDISABLE_FLL */
#define portTASK_INITIAL_R2 ( ( portBASE_TYPE ) GIE )
#endif /* portDISABLE_FLL */

/* Constants required for hardware setup.  The tick ISR runs off the ACLK, 
not the MCLK. */
#define portACLK_FREQUENCY_HZ			( ( TickType_t ) 32768 )
#define portINITIAL_CRITICAL_NESTING	( ( uint16_t ) 10 )
#define portFLAGS_INT_ENABLED	( ( StackType_t ) 0x08 )

/* We require the address of the pxCurrentTCB variable, but don't want to know
any details of its type. */
typedef void TCB_t;
extern volatile TCB_t * volatile pxCurrentTCB;

/* Most ports implement critical sections by placing the interrupt flags on
the stack before disabling interrupts.  Exiting the critical section is then
simply a case of popping the flags from the stack.  As mspgcc does not use
a frame pointer this cannot be done as modifying the stack will clobber all
the stack variables.  Instead each task maintains a count of the critical
section nesting depth.  Each time a critical section is entered the count is
incremented.  Each time a critical section is left the count is decremented -
with interrupts only being re-enabled if the count is zero.

usCriticalNesting will get set to zero when the scheduler starts, but must
not be initialised to zero as this will cause problems during the startup
sequence. */
volatile uint16_t usCriticalNesting = portINITIAL_CRITICAL_NESTING;
/*-----------------------------------------------------------*/

#if __MSP430X__ & ( __MSP430_CPUX_TARGET_SR20__ | __MSP430_CPUX_TARGET_ISR20__ )
/* Save 20-bit registers if somebody seems to be using 20-bit code.
 * Yield in the context of 20-bit code may require manipulating
 * the stack, which requires a scratch register, so portASM_PUSH_GEN_REGS_TAIL()
 * completes the operation of portASM_PUSH_GEN_REGS assuming that r15
 * has been pushed to the stack top. */

#define portASM_PUSH_GEN_REGS					\
	"pushm.a	#12, r15	\n\t"
#define portASM_PUSH_GEN_REGS_TAIL				\
	"pushm.a	#11, r14	\n\t"
#define portASM_POP_GEN_REGS					\
	"popm.a		#12, r15	\n\t"

#elif __MSP430X__
/* Save 16-bit registers if nobody seems to be using 20-bit code */

#define portASM_PUSH_GEN_REGS					\
	"pushm.w	#12, r15	\n\t"
#define portASM_POP_GEN_REGS					\
	"popm.w		#12, r15	\n\t"

#else

#define portASM_PUSH_GEN_REGS   \
            "push r15   \n\t"   \
            "push r14   \n\t"   \
            "push r13   \n\t"   \
            "push r12   \n\t"   \
            "push r11   \n\t"   \
            "push r10   \n\t"   \
            "push r9    \n\t"   \
            "push r8    \n\t"   \
            "push r7    \n\t"   \
            "push r6    \n\t"   \
            "push r5    \n\t"   \
            "push r4    \n\t"

#define portASM_POP_GEN_REGS    \
            "pop r4     \n\t"   \
            "pop r5     \n\t"   \
            "pop r6     \n\t"	\
            "pop r7     \n\t"	\
            "pop r8     \n\t"	\
            "pop r9     \n\t"	\
            "pop r10    \n\t"	\
            "pop r11    \n\t"	\
            "pop r12    \n\t"	\
            "pop r13    \n\t"	\
            "pop r14    \n\t"	\
            "pop r15    \n\t"					
						   
#endif

#if __MSP430X__ & __MSP430_CPUX_TARGET_D20__

#define portASM_STORE_CONTEXT		\
	"pushx.w    %0		\n\t"   \
	"mova       %1, r12	\n\t"	\
	"mova       r1, @r12    \n\t"

#define portASM_RECALL_CONTEXT      \
	"mova	%1, r12     \n\t"   \
	"mova	@r12, r1    \n\t"   \
	"popx.w %0          \n\t"

#else

#define portASM_STORE_CONTEXT       \
	"push	%0          \n\t"   \
	"mov.w	%1, r12     \n\t"   \
	"mov.w	r1, @r12    \n\t"

#define portASM_RECALL_CONTEXT      \
	"mov.w	%1, r12     \n\t"   \
	"mov.w	@r12, r1    \n\t"   \
	"pop	%0          \n\t"

#endif

/* 
 * Macro to save a task context to the task stack.  This simply pushes all the 
 * general purpose msp430 registers onto the stack, followed by the 
 * usCriticalNesting value used by the task.  Finally the resultant stack 
 * pointer value is saved into the task control block so it can be retrieved 
 * the next time the task executes.
 */
#define portSAVE_CONTEXT()                                  \
    __asm__ __volatile__ (  portASM_PUSH_GEN_REGS           \
                            portASM_STORE_CONTEXT           \
                            :                               \
                            : "m"( usCriticalNesting ),     \
                            "m"( pxCurrentTCB )             \
                                                        )

#define portSAVE_CONTEXT_TAIL()                             \
    __asm__ __volatile__ (  portASM_PUSH_GEN_REGS_TAIL      \
                            portASM_STORE_CONTEXT           \
                            :                               \
                            : "m"( usCriticalNesting ),     \
                            "m"( pxCurrentTCB )             \
                                                        )
/* 
 * Macro to restore a task context from the task stack.  This is effectively
 * the reverse of portSAVE_CONTEXT().  First the stack pointer value is
 * loaded from the task control block.  Next the value for usCriticalNesting
 * used by the task is retrieved from the stack - followed by the value of all
 * the general purpose msp430 registers.
 *
 * The bic instruction ensures there are no low power bits set in the status
 * register that is about to be popped from the stack.
 */
#define portRESTORE_CONTEXT()					\
	__asm__ __volatile__ (  portASM_RECALL_CONTEXT		\
                                portASM_POP_GEN_REGS		\
                                "bic	%2, @r1	 \n\t"          \
                                "reti		 \n\t"          \
                                : "=m"( usCriticalNesting )	\
                                : "m"( pxCurrentTCB ),		\
                                      "i"( portLPM_bits )       \
                                );
/*-----------------------------------------------------------*/

/*
 * Sets up the periodic ISR used for the RTOS tick.  This uses timer 0, but
 * could have alternatively used the watchdog timer or timer 1.
 */
static void prvSetupTimerInterrupt( void );
/*-----------------------------------------------------------*/



typedef struct xTCB
{
    BaseType_t usCriticalNesting;
    SavedRegister_t uxRegisters[12];
    unsigned short usPChiSR;
    unsigned short usPClow;
} xTCB;

/* 
 * Initialise the stack of a task to look exactly as if a call to 
 * portSAVE_CONTEXT had been called.
 * 
 * See the header file portable.h.
 */
portSTACK_TYPE *pxPortInitialiseStack( StackType_t *pxTopOfStack, TaskFunction_t pxCode, void *pvParameters )
{
    unsigned short usRegisterIndex;
    xTCB *pxTCB = ( xTCB * ) pxTopOfStack;
    pxTCB--;

    /* The msp430 automatically pushes the PC then SR onto the stack
    before executing an ISR.  When the PC is 20 bit, the upper 4 bits
    are stored in the upper bits of the SR word.  We want the stack to
    look just as if this has happened so place a pointer to the start
    of the task on the stack first - followed by the flags we want the
	task to use when it starts up. */
#if __MSP430X__ & __MSP430_CPUX_TARGET_C20__
    pxTCB->usPClow = ( unsigned short ) ( uint20_t ) pxCode;
    pxTCB->usPChiSR = ( 0xF000 & ( unsigned short ) ( ( ( uint20_t ) pxCode ) >> 4 ) );
    pxTCB->usPChiSR |= portTASK_INITIAL_R2;
#else
    pxTCB->usPClow = ( unsigned short ) pxCode;
    pxTCB->usPChiSR = portTASK_INITIAL_R2;
#endif

    /* When the task starts is will expect to find the function parameter in
    R15. */
    pxTCB->uxRegisters[11] = ( SavedRegister_t ) ( uintptr_t ) pvParameters;

    /* Next the remaining general purpose registers: 4 through 14. */
    for (usRegisterIndex = 4; usRegisterIndex <= 14; ++usRegisterIndex)
        pxTCB->uxRegisters[usRegisterIndex - 4] = usRegisterIndex;

    /* The code generated by the mspgcc compiler does not maintain separate
    stack and frame pointers. The portENTER_CRITICAL macro cannot therefore
    use the stack as per other ports.  Instead a variable is used to keep
    track of the critical section nesting.  This variable has to be stored
    as part of the task context and is initially set to zero. */
    pxTCB->usCriticalNesting = ( portBASE_TYPE ) portNO_CRITICAL_SECTION_NESTING;	

    /* Return a pointer to the top of the stack we have generated so this can
    be stored in the task control block for the task. */
    return ( portSTACK_TYPE * )pxTCB;
}

/*-----------------------------------------------------------*/

BaseType_t xPortStartScheduler( void )
{
    /* Setup the hardware to generate the tick.  Interrupts are disabled when
    this function is called. */
    prvSetupTimerInterrupt();

    /* Restore the context of the first task that is going to run. */
    portRESTORE_CONTEXT();

    /* Should not get here as the tasks are now running! */
    return pdTRUE;
}
/*-----------------------------------------------------------*/

void vPortEndScheduler( void )
{
    /* It is unlikely that the MSP430 port will get stopped.  If required simply
    disable the tick interrupt here. */
}
/*-----------------------------------------------------------*/

/*
 * Manual context switch called by portYIELD or taskYIELD.  
 *
 * The first thing we do is save the registers so we can use a naked attribute.
 */
void vPortYield( void ) __attribute__ ( ( naked ) );
void vPortYield( void )
{
    /* We want the stack of the task being saved to look exactly as if the task
    was saved during a pre-emptive RTOS tick ISR.  Before calling an ISR the 
    msp430 places the status register onto the stack.  As this is a function 
    call and not an ISR we have to do this manually. */
    asm volatile ( "push r2 \n\t" );
    _DINT();

    /* Save the context of the current task. */
    portSAVE_CONTEXT();

    /* Switch to the highest priority task that is ready to run. */
    vTaskSwitchContext();

    /* Restore the context of the new task. */
    portRESTORE_CONTEXT();
}
/* The TI headers are inconsistent as to whether the first Timer_A is
   controlled via TACTL or TA0CTL. */
#if ! defined(TACTL_) && ! defined(TACTL)
#define TACTL TA0CTL
#define TAR TA0R
#define TACCR0 TA0CCR0
#define TACCTL0 TA0CCTL0
#endif /* TACTL */
#ifndef TIMERA0_VECTOR
#define TIMERA0_VECTOR TIMER0_A0_VECTOR
#endif /* TIMERA0_VECTOR */

/*-----------------------------------------------------------*/

#ifndef portCLK_SOURCE
#define portCLK_SOURCE 0
#warning "Clock source for tick is ACK."
#endif

#if ( portCLK_SOURCE == 0 )
#define TASSEL_X        TASSEL_1
#elif ( portCLK_SOURCE == 1 )
#define TASSEL_X        TASSEL_2
#else
#error Unknown clock source for timer.
#endif

/*-----------------------------------------------------------*/

#ifndef portCLK_SOURCE_FREQUENCY_HZ
#define portCLK_SOURCE_FREQUENCY_HZ 32000UL
#warning "It is assumed that the frequency of the clock source for tick is 32kHz."
#endif

#if ( ( (portCLK_SOURCE_FREQUENCY_HZ/(1ULL*configTICK_RATE_HZ)) +1 ) <= 0xffffULL )
#define ID_X          ID_0
#define TACCR0_VALUE  ((portCLK_SOURCE_FREQUENCY_HZ/(1ULL*configTICK_RATE_HZ))+1)
#elif (((1ULL*portCLK_SOURCE_FREQUENCY_HZ/(2ULL*configTICK_RATE_HZ))+1) <= 0xffffULL)
#define ID_X          ID_1
#define TACCR0_VALUE  ((portCLK_SOURCE_FREQUENCY_HZ/(2ULL*configTICK_RATE_HZ))+1)
#elif (((1ULL*portCLK_SOURCE_FREQUENCY_HZ/(4ULL*configTICK_RATE_HZ))+1) <= 0xffffULL)
#define ID_X          ID_2
#define TACCR0_VALUE  ((portCLK_SOURCE_FREQUENCY_HZ/(4ULL*configTICK_RATE_HZ))+1)
#elif (((1ULL*portCLK_SOURCE_FREQUENCY_HZ/(8ULL*configTICK_RATE_HZ))+1) <= 0xffffULL)
#define ID_X          ID_3
#define TACCR0_VALUE  ((portCLK_SOURCE_FREQUENCY_HZ/(8ULL*configTICK_RATE_HZ))+1)
#else
#error "The configurations of portCLK_SOURCE_FREQUENCY_HZ and configTICK_RATE_HZ are not possible."
#define ID_X          ID_0
#endif

/*-----------------------------------------------------------*/
/*
 * Hardware initialisation to generate the RTOS tick.  This uses timer 0
 * but could alternatively use the watchdog timer or timer 1. 
 */
static void prvSetupTimerInterrupt( void )
{
    /* Ensure the timer is stopped before configuring. */
    TACTL = 0;
    __delay_cycles(50);
    
    /* Count continuously clearing the initial counter. */
    TACTL = ( TASSEL_X | MC_1 | TACLR | ID_X );    
        
    /* Set the compare match value according to the tick rate we want. */
    TACCR0 = TACCR0_VALUE;

    /* Enable the interrupts. */
    TACCTL0 = CCIE;
}

/*-----------------------------------------------------------*/

/* 
 * The interrupt service routine used depends on whether the pre-emptive
 * scheduler is being used or not.
 */

#if configUSE_PREEMPTION == 1

    /*
     * Tick ISR for preemptive scheduler.  We can use a naked attribute as
     * the context is saved at the start of vPortYieldFromTick().  The tick
     * count is incremented after the context is saved.
     */
    interrupt (TIMERA0_VECTOR) prvTickISR( void ) __attribute__ ( ( naked ) );
    interrupt (TIMERA0_VECTOR) prvTickISR( void )
    {
        /* Save the context of the interrupted task. */
        portSAVE_CONTEXT();

        /* Increment the tick count then switch to the highest priority task
        that is ready to run. */
        if( xTaskIncrementTick() != pdFALSE )
        {
            vTaskSwitchContext();
        }

        /* Restore the context of the new task. */
        portRESTORE_CONTEXT();
    }

#else

    /*
     * Tick ISR for the cooperative scheduler.  All this does is increment the
     * tick count.  We don't need to switch context, this can only be done by
     * manual calls to taskYIELD();
     */
    interrupt (TIMERA0_VECTOR) prvTickISR( void );
    interrupt (TIMERA0_VECTOR) prvTickISR( void )
    {
        xTaskIncrementTick();
    }
#endif


	

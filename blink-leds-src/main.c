
/* ------------------------------------------------------------------------ */
/** @file main.c
  * @brief  FreeRTOS demo application for msp-exp430g2 board. 
  * @date   12/10/2015.
  * @author Rafa García.                                                    */
/* ------------------------------------------------------------------------ */

#include <intrinsics.h>
#include <msp430.h>
#include "FreeRTOS.h"
#include "task.h"

/** LED red of msp-exp430 board. */
#define LED_RED     BIT0 

/** LED green of msp-exp430 board. */
#define LED_GREEN   BIT6

/** Output port for LEDs. */
#define LED_OUT     P1OUT

/** Configuration port of LEDs. */
#define LED_DIR     P1DIR

/** The preprocessor converts from seconds to timer ticks. */
#define __sec( t ) ((const TickType_t)( t * configTICK_RATE_HZ ))

/** Periodically turn on the red LED. */
void vTurnOnRed( void *pvParameters ) {
    while (1) {
        LED_OUT |= LED_RED;
        vTaskDelay( __sec(0.5) );
    }
}

/** Periodically turn off the red LED. */
void vTurnOffRed( void *pvParameters ) {
    vTaskDelay( __sec(0.25) );
    while (1) {
        LED_OUT &= (~LED_RED);
        vTaskDelay( __sec(0.5) );
    }
}

/** Periodically turn on the green LED. */
void vTurnOnGreen( void *pvParameters ) {
    vTaskDelay( __sec(0.125) );
    while (1) {
        LED_OUT |= LED_GREEN;
        vTaskDelay( __sec(0.5) );
    }
}

/** Periodically turn off the green LED. */
void vTurnOffGreen( void *pvParameters ) {
    vTaskDelay( __sec(0.25) );
    while (1) {
        LED_OUT &= (~LED_GREEN);
        vTaskDelay( __sec(0.5) );
    }
}

/** Called on each iteration of the idle task.  In this case the idle task
    just enters a low(ish) power mode. */
void vApplicationIdleHook( void ) {
    __bis_SR_register( LPM0_bits | GIE );
}

/** Entry point of application. */
int main( void ) {    
    
    /* Config watch dog timer: */
    WDTCTL = WDTPW | WDTHOLD;

    /* Config MCU clock: */
    BCSCTL1 = CALBC1_1MHZ; 
    DCOCTL = CALDCO_1MHZ;  

    /* Config ports: */
    LED_OUT &= ~(LED_RED + LED_GREEN); 
    LED_DIR |= (LED_RED + LED_GREEN); 

    /* Create all tasks: */
    xTaskCreate(  vTurnOnRed, (char *) "Turn on the red LED.",
                  configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
                  (xTaskHandle *) NULL );

    xTaskCreate(  vTurnOffRed, (char *) "Turn off the red LED.",
                  configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
                  (xTaskHandle *) NULL );

    xTaskCreate(  vTurnOnGreen, (char *) "Turn on the green LED.",
                  configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
                  (xTaskHandle *) NULL );

    xTaskCreate(  vTurnOffGreen, (char *) "Turn off the green LED.",
                  configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
                  (xTaskHandle *) NULL );

    /* Start the scheduler: */
    vTaskStartScheduler(); 
    
    /* If you see the two LEDs turned on it means there is not enough memory. */
    LED_OUT |= ( LED_RED | LED_GREEN ); 
    for(;;);
    
    return 0;   
} 

/* ------------------------------------------------------------------------ */

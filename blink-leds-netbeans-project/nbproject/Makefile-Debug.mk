#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=msp430-gcc
CCC=msp430-g++
CXX=msp430-g++
FC=gfortran
AS=msp430-as

# Macros
CND_PLATFORM=msp430-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/783286892/list.o \
	${OBJECTDIR}/_ext/2112081171/port.o \
	${OBJECTDIR}/_ext/714316809/heap_1.o \
	${OBJECTDIR}/_ext/783286892/queue.o \
	${OBJECTDIR}/_ext/783286892/tasks.o \
	${OBJECTDIR}/_ext/1274105311/main.o


# C Compiler Flags
CFLAGS=-mmcu=msp430g2553 -ffunction-sections -std=gnu99

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/blink-leds-netbeans-project

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/blink-leds-netbeans-project: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/blink-leds-netbeans-project ${OBJECTFILES} ${LDLIBSOPTIONS} -Wl,--gc-sections

${OBJECTDIR}/_ext/783286892/list.o: ../FreeRTOSv8.2.2/FreeRTOS/Source/list.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/783286892
	${RM} "$@.d"
	$(COMPILE.c) -g -Werror -D__MSP430G2553__ -I../FreeRTOSv8.2.2/FreeRTOS/Source/include -I../FreeRTOSv8.2.2/FreeRTOS/Source/portable/GCC/MSP430G2 -I../blink-leds-src -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/783286892/list.o ../FreeRTOSv8.2.2/FreeRTOS/Source/list.c

${OBJECTDIR}/_ext/2112081171/port.o: ../FreeRTOSv8.2.2/FreeRTOS/Source/portable/GCC/MSP430G2/port.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/2112081171
	${RM} "$@.d"
	$(COMPILE.c) -g -Werror -D__MSP430G2553__ -I../FreeRTOSv8.2.2/FreeRTOS/Source/include -I../FreeRTOSv8.2.2/FreeRTOS/Source/portable/GCC/MSP430G2 -I../blink-leds-src -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/2112081171/port.o ../FreeRTOSv8.2.2/FreeRTOS/Source/portable/GCC/MSP430G2/port.c

${OBJECTDIR}/_ext/714316809/heap_1.o: ../FreeRTOSv8.2.2/FreeRTOS/Source/portable/MemMang/heap_1.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/714316809
	${RM} "$@.d"
	$(COMPILE.c) -g -Werror -D__MSP430G2553__ -I../FreeRTOSv8.2.2/FreeRTOS/Source/include -I../FreeRTOSv8.2.2/FreeRTOS/Source/portable/GCC/MSP430G2 -I../blink-leds-src -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/714316809/heap_1.o ../FreeRTOSv8.2.2/FreeRTOS/Source/portable/MemMang/heap_1.c

${OBJECTDIR}/_ext/783286892/queue.o: ../FreeRTOSv8.2.2/FreeRTOS/Source/queue.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/783286892
	${RM} "$@.d"
	$(COMPILE.c) -g -Werror -D__MSP430G2553__ -I../FreeRTOSv8.2.2/FreeRTOS/Source/include -I../FreeRTOSv8.2.2/FreeRTOS/Source/portable/GCC/MSP430G2 -I../blink-leds-src -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/783286892/queue.o ../FreeRTOSv8.2.2/FreeRTOS/Source/queue.c

${OBJECTDIR}/_ext/783286892/tasks.o: ../FreeRTOSv8.2.2/FreeRTOS/Source/tasks.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/783286892
	${RM} "$@.d"
	$(COMPILE.c) -g -Werror -D__MSP430G2553__ -I../FreeRTOSv8.2.2/FreeRTOS/Source/include -I../FreeRTOSv8.2.2/FreeRTOS/Source/portable/GCC/MSP430G2 -I../blink-leds-src -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/783286892/tasks.o ../FreeRTOSv8.2.2/FreeRTOS/Source/tasks.c

${OBJECTDIR}/_ext/1274105311/main.o: ../blink-leds-src/main.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1274105311
	${RM} "$@.d"
	$(COMPILE.c) -g -Werror -D__MSP430G2553__ -I../FreeRTOSv8.2.2/FreeRTOS/Source/include -I../FreeRTOSv8.2.2/FreeRTOS/Source/portable/GCC/MSP430G2 -I../blink-leds-src -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1274105311/main.o ../blink-leds-src/main.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/blink-leds-netbeans-project

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
